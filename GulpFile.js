var gulp = require('gulp')
  , less = require('gulp-less')
  , jade = require('gulp-jade');

var livereload = require('gulp-livereload');


gulp.task('jade', function () {
  gulp.src(['./src/jade/index.jade','./src/jade/insta.jade','./src/jade/rules.jade'])
    .pipe(jade({
      pretty : true
    }))
    .pipe(gulp.dest('./dist/new'))
    .pipe(livereload());
});

gulp.task('js', function () {
  gulp.src('./src/js/**/*.js')
    .pipe(gulp.dest('./dist/new/js'))
    .pipe(livereload());
});

gulp.task('less', function () {
  gulp.src('./src/less/index.less')
    .pipe(less())
    .pipe(gulp.dest('./dist/new/css'))
    .pipe(livereload());
});

gulp.task('default', function () {
  livereload.listen();
  gulp.start(['less', 'js', 'jade']);

  gulp.watch('./src/jade/**/*.jade', ['jade']);
  gulp.watch('./src/js/**/*.js', ['js']);
  gulp.watch('./src/less/**/*.less', ['less']);
});