document.addEventListener("DOMContentLoaded", function (event) {
  'use strict';

  var url = "./response.json";

  var form = document.getElementById('form');

  if (form)
    form.addEventListener('click', function (event) {
      var target = event.target;

      while (target && target != this && !target.classList.contains('project-add') && !target.classList.contains('project-remove')) {
        target = target.parentNode;
      }

      if (target.classList.contains('project-add'))
        return addProjectInput();

      if (target.classList.contains('project-remove'))
        return removeProject(target);

    });

  function setLastPlus() {
    var projects = [].slice.call(form.querySelectorAll('.project'));
    projects.forEach(function (el, i, arr) {
      if (i < arr.length - 1) {
        var pl = el.querySelector('.project-add');
        if (pl) {
          pl.className = "project-remove";
          pl.innerHTML = "<span>-</span>";
        }
      } else {
        var min = el.querySelector('.project-remove');
        if (min) {
          min.className = "project-add";
          min.innerHTML = "<span>+</span>";
        }
      }
    });
  }

  function addProjectInput() {

    var projects = [].slice.call(form.querySelectorAll('.project'))
      , last = projects[projects.length - 1];

    var newProject = document.createElement('div');
    newProject.className = 'project';

    newProject.innerHTML = '<input type="text" name="projects[]" placeholder="Ссылка на ваш профиль в соц. сети или блог"><a class="project-add"><span>+</span></a>';

    last.parentNode.insertBefore(newProject, last.nextSibling);
    setLastPlus();
  }

  function removeProject(target) {
    while (target && target != this && !target.classList.contains('project')) {
      target = target.parentNode;
    }

    if (target.classList.contains('project')) {
      target.parentNode.removeChild(target);
    }
    setLastPlus();
  }

  //document.querySelector('.section-six').addEventListener('click', function () {
  //  this.classList.toggle('show-social');
  //});
  //
  function showSocialPopup(form) {
    var block = form.parentNode;
    while (block && block != document.body && !block.classList.contains('block')) {
      block = block.parentNode;
    }
    if (!block.classList.contains('block'))
      return;
    block.classList.toggle('show-social');
  }

  //CHECK FORM

  function isNotEmpty(input) {
    return (input.value.trim() != '');
  }

  function isNormLength(text) {
    return text.trim().length > 1;
  }

  function isEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  function addError(input) {
    input.classList.add('error');
    input.classList.remove('success');
  }

  function addSuccess(input) {
    input.classList.remove('error');
    input.classList.add('success');
  }

  function clearInput(input) {
    input.classList.remove('error');
    input.classList.remove('success');
  }

  function checkInput(cond, input) {
    cond ? addSuccess(input) : addError(input);
    return cond;
  }

  [].slice.call(document.querySelectorAll('.form-register')).forEach(function (form) {
    form.addEventListener('input', function (event) {
      clearInput(event.target);
    });
    form.addEventListener('submit', function (event) {
      event.stopPropagation();
      event.preventDefault();
      //name
      var projects_input = 0;
      var hasError = false;
      var data = {};
      for (var i = 0; i < this.length; i++) {
        if (this[i].type == "text") {
          var input = this[i];
          switch (input.name) {
            case "name":
            case "surname":
              data[input.name] = input.value.trim();
              hasError = (!checkInput((isNotEmpty(input) && isNormLength(input.value)), input) || hasError);
              break;
            case "email":
              data.email = input.value.trim();
              hasError = (!checkInput((isNotEmpty(input) && isNormLength(input.value) && isEmail(input.value)), input) || hasError);
              break;
            case "projects[]":
              if (projects_input > 0) {
                data.projects.push(input.value.trim());
                break;
              } else {
                data.projects = [input.value.trim()];
                projects_input++;
                hasError = (!checkInput((isNotEmpty(input) && isNormLength(input.value)), input) || hasError);
                break;
              }
          }
        }
      }
      if (!hasError)
        submitForm(this, data);
    });
  });

  function submitForm(form, data) {
    $.ajax(url, {
      method   : "post",
      data     : data,
      dataType : "json"
    }).done(function (response) {
      // Все хорошо показываем попап
      if (response && response.success) {
        showSocialPopup(form);
      }
    });
  }
});