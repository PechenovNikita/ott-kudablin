document.addEventListener("DOMContentLoaded", function (event) {
  'use strict';
  /**
   *
   * @param modal
   * @returns {number}
   */
  function getModalHeight(modal) {
    return [].slice.call(modal.children).reduce(function (last, child) {
      var position = getComputedStyle(child).position;
      if (['absolute', 'fixed'].indexOf(position) > -1) {
        return last;
      }
      return last + child.offsetHeight;
    }, 0);
  }

  function showModal(btn) {
    var block = btn;
    while (block && block != document.body && !block.classList.contains('block')) {
      block = block.parentNode;
    }

    if (!block.classList.contains('block'))
      return false;

    if (btn.hasAttribute('data-modal-height')) {
      var modal = block.querySelector('.modal');
      var innerHeight = getModalHeight(modal);
      modal.style.height = innerHeight + "px";

      setTimeout(function () {
        modal.style.height = '';
      }, 300);
    }

    block.classList.add('show-modal');
    return block;
  }

  function hideModal(block) {
    var btn = block.querySelector('.modal-open');
    console.log(btn);
    if (btn && btn.hasAttribute('data-modal-height')) {
      var modal = block.querySelector('.modal');
      var innerHeight = getModalHeight(modal);
      console.log(innerHeight);
      modal.style.height = innerHeight + "px";

      setTimeout(function () {
        modal.style.height = '';
      }, 10);
    }

    block.classList.remove('show-modal');
  }


  [].slice.call(document.querySelectorAll('.modal-open')).forEach(function (el) {
    el.addEventListener('click', function (event) {
        var block = showModal(this);
        if (block) {
          event.stopPropagation();
          event.preventDefault();

          var modal = block.querySelector('.modal');
          //console.log(modal, modal.offsetTop);

          smooth_scroll_to(document.body, (modal), 500);
        }
      }
    );
  });

  [].slice.call(document.querySelectorAll('.block')).forEach(function (el) {
    el.addEventListener('click', function (event) {
        var target = event.target;
        while (target && (target != document.body) && !target.classList.contains('block') && !target.classList.contains('modal')) {
          target = target.parentNode;
        }
        if (!target || (target == document.body) || target.classList.contains('modal'))
          return true;

        if (this.classList.contains('show-modal')) {
          event.stopPropagation();
          event.preventDefault();
          hideModal(this);
        }
      }
    );
  });

  document.addEventListener('keydown', function (event) {
    if (event.keyCode === 27) {
      [].slice.call(document.querySelectorAll('.show-modal')).forEach(function (el) {
        hideModal(el);
      });
    }
  });
});