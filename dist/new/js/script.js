var bgItem = document.querySelectorAll('[data-scrollbg]');
var sectionArray = [].slice.call(bgItem);
var mainblock = document.querySelector('div.main-container');
var activeItem = document.getElementsByClassName('active');

function semiParalax() {
  var fromTopY = window.pageYOffset;

  (function currentBlock() {
    for (var i = 0; i < sectionArray.length; i++) {
      if (sectionArray[i].offsetTop <= (parseInt(fromTopY) + sectionArray[i].clientHeight / 2)) {
        // sectionArray[i].offsetTop === 0 || sectionArray[i].offsetTop < (parseInt(fromTopY) + (sectionArray[i].offsetTop / 2)) &&
        // 				sectionArray[i].offsetTop > (fromTopY - (sectionArray[i].offsetTop / 10))
        sectionArray[i].classList.add('active');

      }
      else if (sectionArray[i].offsetTop + sectionArray[i].clientHeight) {
        sectionArray[i].classList.remove('active');
      }
    }
  }());

  //currentBlock();

  (function actionFn() {
    for (var i = 0; i < activeItem.length; i++) {
      var blockHeight = activeItem[i].clientHeight;
      if (fromTopY !== blockHeight) {
        activeItem[i].style.backgroundPosition = '0px ' + parseInt(fromTopY) / 10 + 'px';
      }

    }
  }());

  //actionFn();

}

window.addEventListener('scroll', function () {
  semiParalax()
});


